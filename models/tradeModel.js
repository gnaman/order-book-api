const mongoose = require('mongoose');


//Schema for trade document
const tradeSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    qty: {
        type: Number,
        min: [1, "You can't trade for 0 or negative no of stocks"]
    },
    tickr: {
        type: String,
        required: true,
        uppercase: true
    },
    type_of_trade: {
        type: String,
        uppercase: true,
        enum: ['BUY', 'SELL'],
        message: "Only BUY and SELL are allowed",
        required: true
    },
    price: {
        type: Number,
        required: true
    }
});

module.exports = mongoose.model('Trade', tradeSchema);