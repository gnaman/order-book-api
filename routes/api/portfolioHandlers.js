//Porfolio handlers are all methods and route handlers
//related to portfolio, its holdings and returns on the portfolio

var router = require('express').Router();

const Trade = require('../../models/tradeModel');


router.get('/', (req, res, next) => {
	// console.log(req.query);

	getPortfolio().then(portfolio => {
		// console.log(portfolio);
		res.send(portfolio);
	}).catch(e => {
		res.status(500);
		res.send(e.message);
		return next(new Error(e.message));
	});
});

router.get('/holdings', (req, res, next) => {

	//resolves the promise returned by getHoldings() to obtain net holdings
	getHoldings().then(holdings => {
		res.send(holdings)
	}).catch(err => {
		res.status(500);
		res.send(err.message);
		return next(new Error(err.message));
	})
});

router.get('/returns', (req, res) => {
	const current_price = req.query.current_price || 100;


	//Uses holdings to calculate cummulative returns with the current price
	//being set from either the query params or a default of 100
	getHoldings().then(holdings => {
		cumm_return = 0
		// console.log(holdings);
		holdings.map(security => {
			// console.log(security);
			cumm_return += (current_price - security['avg_bprice']) * security['final_qty'];
		})

		return res.json({"return" : cumm_return})
	}).catch(err => {
		res.status(500);
		res.send(err.message);
		return next(new Error(err.message));
	});
})


async function getPortfolio(){

	//Aggregation query pipeline to return a list of securities with associated trades
	//along with `net_stocks` attribute depicting the quantity of shares as a
	//difference of what you bought vs what you sold

	//returns a promise which can be resolved later
	return Trade.aggregate([
		{
			"$project": {
				"_id": 1,
				"tickr" : 1,
				"qty" : 1,
				"price" : 1,
				"type_of_trade" : 1,
				"stocks_bought" : {
					"$cond": { if: { "$eq": [ "$type_of_trade", "BUY" ] }, then: {"$sum" : "$qty"}, else: {"$sum" : "0"} }
				},
				"stocks_sold" : {
					"$cond": { if: { "$eq": [ "$type_of_trade", "SELL" ] }, then: {"$sum" : "$qty"}, else: {"$sum" : "0"} }
				}
			}
		},
		{
			"$group" : {
				"_id" : "$tickr",
				"sum_ss" : {
					"$sum" : "$stocks_sold"
				},
				"sum_sb" : {
					"$sum" : "$stocks_bought"
				},
				"trades" : {
					"$addToSet" : {
						"_id" : "$_id",
						"price" : "$price",
						"qty" : "$qty",
						"type_of_trade" : "$type_of_trade",
						"tickr": "$tickr"
					}
				}
			}
		},
		{
			"$project" : {
				"tickr" : 1,
				"trades" : 1,
				"net_stocks" : {
					"$cond" : [
						{"$gt" :[{"$subtract" : [ "$sum_sb", "$sum_ss" ]}, 0]},
						{"$subtract" : [ "$sum_sb", "$sum_ss" ]},
						0
					]
				}
			}
		},
		{
			"$match" :{
				"net_stocks" : {"$gt" : 0}
			}
		}
	]);
}

async function getHoldings(){
	try{

		//uses the getPortfolio promise to calculate average buy price per security
		portfolio = await getPortfolio();
		holdings = []
		for(i in portfolio){
			hldng = {"tickr": portfolio[i]._id, "final_qty" : portfolio[i].net_stocks}
			var sum = 0, qty = 0;
			for (j in portfolio[i]['trades']){
				if(portfolio[i]['trades'][j]['type_of_trade'] == "BUY"){
					sum += (portfolio[i]['trades'][j]['qty'] * portfolio[i]['trades'][j]['price']);
					qty += portfolio[i]['trades'][j]['qty'];
				}
			}
		let avg_bprice = sum / qty;
		hldng['avg_bprice'] = avg_bprice;
		holdings.push(hldng);
		}

		//holdings contain a list of securties along with its average buy price and final quantity
		return holdings;
	} catch(err) {
		throw(err);
	}


}

module.exports = router;
