var router = require('express').Router();

var tradeHandlers = require('./tradeHandlers');
var portfolioHandlers = require('./portfolioHandlers');

router.use('/trade', tradeHandlers);
router.use('/portfolio', portfolioHandlers);

router.use('/', (req, res) => {
	res.send('HELLO FROM THE API');
});

module.exports = router;