var router = require('express').Router()
const mongoose = require('mongoose');

const Trade = require('../../models/tradeModel');

router.post('/place-trade', async (req, res, next) => {
	// console.log(req.body);


	//constructs a new trade model with the received parameters

	try{
		var trade = new Trade({
			_id: new mongoose.Types.ObjectId(),
			qty: req.body.qty,
			tickr: req.body.tickr,
			type_of_trade: req.body.type_of_trade,
			price: req.body.price
		});
	} catch(e){
		res.status(422);
		res.send("Check body parameters");
		return next(new Error(e.message));
	}


	if(trade.type_of_trade === "SELL"){
		let availability = await checkAvailability(trade.tickr);
		if(availability[0]._id === "SELL")
			qty_left = availability[1].stocks - availability[0].stocks;
		if(availability.length == 0 || trade.qty > qty_left){
			res.status(422);
			res.send("You can only sell upto a " + qty_left + " share(s)");
			return next(new Error("You can only sell upto a " + qty_left + " share(s)"));
		}
	}

	trade.save().then(result => {
		return res.send(result)
	}).catch(err => {
		res.status(500)
		res.send(err.message)
	});
});

router.post('/update-trade', (req, res, next) => {
	// console.log(req.body);

	if(!req.body.id){
		res.status(422);
		res.send("Please provide id to continue");
		return next(new Error("id missing from request body"));
	}

	const trade = new Trade({
		_id: new mongoose.Types.ObjectId(req.body.id),
		...req.body
	});

	Trade.findOneAndUpdate({_id : new mongoose.Types.ObjectId(req.body.id)}, trade, {upsert: true})
		.then(result => {
			res.status(200);
			res.send(result);
		})
		.catch(err => {
				res.status(500);
				res.send(err.message);
		});

});

router.post('/remove-trade', (req, res, next) => {

	if(!req.body.id){
		res.status(422);
		res.send("Please provide id to continue");
		return next(new Error("id missing from request body"));
	}

	Trade.findByIdAndRemove(req.body.id)
	.then(result => {
		res.status(200);
		res.send(result);
	})
	.catch(err => {
		res.status(500);
		// console.log(err);
		res.send(err.message);
	});

});

function checkAvailability(tickr){
	return Trade.aggregate([
		{
			"$match" : {
				"tickr" : tickr,
			},
		},


		// {
		// 	"$project" : {
		// 		"_id" : "$tickr",
		// 		"type_of_trade" : 1,
		// 		"stocks_bought" : {
		// 			"$cond": { if: { "$eq": [ "$type_of_trade", "BUY" ] }, then: {"$sum" : "$qty"}, else: {"$sum" : "0"} }
		// 		},
		// 		"stocks_sold" : {
		// 			"$cond": { if: { "$eq": [ "$type_of_trade", "SELL" ] }, then: {"$sum" : "$qty"}, else: {"$sum" : "0"} }
		// 		}
		// 	}
		// },
		// {
		// 	"$group" : {
		// 			"_id" : null,
		// 			"stocks_bought" : {"$sum" : "$stocks_bought"},
		// 			"stocks_sold" : {"$sum" : "$stocks_sold"}
		// 		}
		// },
		// {
		// 	"$project" : {
		// 		"_id": 0,
		// 		"qty_left" : {
		// 			"$subtract" : ["$stocks_bought", "$stocks_sold"]
		// 		}
		// 	}
		// }

		//Two approaches to this.
		{
			"$group" : {
				"_id" : "$type_of_trade",
				"stocks" : {
					"$sum" : "$qty"
				}
			}
		}
	])
}

module.exports = router;