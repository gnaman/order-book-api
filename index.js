var express = require('express');
const bodyParser = require('body-parser');
const app = express();

const mongoose = require('mongoose');

//Mongoose to connect to the database
mongoose.connect(
	'mongodb+srv://naman:' + process.env.MONGO_PWD + '@cluster0-sprrp.mongodb.net/test?retryWrites=true',
	{
		useNewUrlParser: true
	}
)

const PORT = process.env.PORT || 8000;

const Routes = require('./routes');



app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());

app.use('/', Routes);

app.listen(PORT, () => {
	console.log('App listening on ' + PORT);
});

module.exports = app;